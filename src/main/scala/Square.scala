/**
  * Corresponds to Square class with four 0-9 digits in the corners.
  * Corners are ordered in the following way.
  *
  * -------------
  * |  1  |  2  |
  * -------------
  * |  3  |  4  |
  * -------------
  *
  * @param topLeft A value in top left corenr.
  * @param topRight A value in top right corner.
  * @param bottomLeft A value in bottom left corner.
  * @param bottomRight A value in bottom right corner.
  */
class Square(val topLeft: Int, val topRight: Int, val bottomLeft: Int, val bottomRight: Int) {

  override def toString: String = s"Square($topLeft, $topRight, $bottomLeft, $bottomRight)"
}

/**
  * Corresponds to a particular cell in the field of squares.
  * i and j values denote coordinates at the field and value contains a Square.
  *
  * @param i row coordinate in the field.
  * @param j column coordinate in the field.
  * @param value Square entity, which corresponds to a cell
  */
class FieldCell(val i: Int, val j: Int, val value: Square)  {

  override def toString: String = s"FieldCell($i, $j, $value)"
}

/**
  * A field consists of several squares. The shape of the field is defined via indexField.
  * IndexField contains a particular cell in row-column intersection, and if the field
  * is not defined in the (row, column) cell, then value in the (row, column) cell is -1.
  * Otherwise there is an index to the correspoding value in the cells list.
  *
  * Example of an indexField:
  *
  * ------------------
  * | -1 |  0  | -1  |
  * ------------------
  * |  1 |  2  |  3  |
  * ------------------
  * | -1 |  4  | -1  |
  * ------------------
  *
  * @param cells list of cells. Order of cells corresponds to the cells order on field (e.g.
  *              0-index cell considered as a first cell in the field, 1-index as a second and etc.)
  *
  * @param indexField An index map of a field.
  */
class Field (val cells: List[FieldCell], val indexField: Array[Array[Int]]) {

  val MAX_SUM: Int = 10

  def sum(first: Option[Square], second: Option[Square], third: Option[Square], fourth: Option[Square]): Int = {
    Seq(first.map(_.bottomRight), second.map(_.bottomLeft), third.map(_.topRight), fourth.map(_.topLeft)).flatten.sum
  }

  /**
    * Validates the 4 adjacent squares.
    * The sum of the adjacent 4 corners should be equal to MAX_SUM,
    * and the sum of N corners, 0 < N < MAX_SUM,
    * should be less or equal to MAX_SUM.
    *
    * The squares are located the following way:
    *-------------
    *|  1  |  2  |
    *-------------
    *|  3  |  4  |
    *-------------
    *
    * @param first first square.
    * @param second second square.
    * @param third third square.
    * @param fourth fourth square.
    * @return boolean, indicating, whether the squares satisfy the conditions.
    * */
  def validate(first: Option[Square], second: Option[Square], third: Option[Square], fourth: Option[Square]):
   Boolean = {
    val sumValue = sum(first, second, third, fourth)
    if (first.isDefined && second.isDefined && third.isDefined && fourth.isDefined)
      sumValue == MAX_SUM
    else sumValue <= MAX_SUM
  }

  /**
    * Obtains all bottom, left and right neighbours of every cell and validates
    *   two corresponding quads of squares and two top corners of the middle and right squares.
    *   The neighbours are located the following way.
    *
    * -----------------------------------
    * |   top    |    top    |    top   |
    * |   left   |           |   right  |
    * -----------------------------------
    * |   left   |   middle  |   right  |
    * |          |           |          |
    * -----------------------------------
    * |  bottom  |   bottom  |   bottom |
    * |   left   |           |   right  |
    * -----------------------------------
    */
  def validate(): Boolean = {
    cells.indices.map(i => {
        val current = cells(i)
        val left = getLeft(current).map(_.value)
        val leftBottom = getLeftBottom(current).map(_.value)
        val bottom = getBottom(current).map(_.value)
        val rightBottom = getRightBottom(current).map(_.value)
        val right = getRight(current).map(_.value)
        validate(Option.empty[Square], Option.empty[Square], Some(current.value), right) &&
        validate(left, Some(current.value), leftBottom, bottom) &&
        validate(Some(current.value), right, bottom, rightBottom)
    }).forall { _ == true }
  }

  /**
    * Tries to obtain an index of a cell from indexField.
    * -1 is returned if field is not defined on (i,j)
    * or if i or j are incorrect.
    *
    * @param i row index
    * @param j col index
    * @return index of a cell in the list, or -1 if nothing is found
    */
  private [this] def index(i: Int, j: Int): Int = {
    indexField.lift(i).map(row => row.lift(j).getOrElse(-1)).getOrElse(-1)
  }

  /**
    * Obtains coordinates of the next not occupied cell
    * on the field.
    *
    * @return tuple of (i, j) coordinates, or (-1, -1)
    *         if nothing is found.
    */
  def nextEmpty(): (Int, Int) = {
     val ind = cells.size
     indexField.indices
       .map(i => (i, indexField(i).indexOf(ind)))
       .filter(inds => inds._2 != -1)
       .toList match {
       case Nil => (-1, -1)
       case head :: tail => head
     }
  }

  /**
    * Obtains the left neighbour of the given cell.
    *
    * @param cell middle cell
    * @return optional value for neighbour
    */
  def getLeft(cell: FieldCell): Option[FieldCell] = {
    cells.lift(index(cell.i, cell.j - 1))
  }

  /**
    * Obtains the right neighbour of the given cell.
    *
    * @param cell middle cell
    * @return optional value for neighbour
    */
  def getRight(cell: FieldCell): Option[FieldCell] = {
    cells.lift(index(cell.i, cell.j + 1))
  }

  /**
    * Obtains the left bottom neighbour of the given cell.
    *
    * @param cell middle cell
    * @return optional value for neighbour
    */
  def getLeftBottom(cell: FieldCell): Option[FieldCell] = {
    cells.lift(index(cell.i + 1, cell.j - 1))
  }

  /**
    * Obtains the bottom neighbour of the given cell.
    *
    * @param cell middle cell
    * @return optional value for neighbour
    */
  def getBottom(cell: FieldCell): Option[FieldCell] = {
    cells.lift(index(cell.i + 1, cell.j))
  }

  /**
    * Obtains the right neighbour of the given cell.
    *
    * @param cell middle cell
    * @return optional value for neighbour
    */
  def getRightBottom(cell: FieldCell): Option[FieldCell] = {
    cells.lift(index(cell.i + 1, cell.j + 1))
  }
}

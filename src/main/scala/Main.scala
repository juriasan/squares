import scala.collection.GenSeq
import scala.io.Source
import scala.util.{Failure, Success, Try}

object Main {

  val USAGE: String = "Usage: main.scala path, path - path to file with squares data. The file " +
    "format should look like the following: \n\n" +
    "a b c d\n" +
    "...\n" +
    "where a, b, c, d - digits from 0 to 9."

  val CANNOT_READ_FILE: String = "Squares data cannot be read or file is empty or corrupted. "
  val TIME: String = "Time is %d ms"

  /**
    * This function sets the each square in squares list
    * to the next empty cell of the field and checks every
    * field whether it's valid or not. Then only valid
    * fields are recursively proceed for further handling
    * with the list of squares reduced by square, which had
    * been set to the field.
    *
    * @param field A field object, which contains a list of squares,
    *              which had been already set to the proper places.
    * @param squares A list of squares, which are left for handling.
    * @return List of possible solutions. In case if squares list is empty,
    *         then a GenSeq of a single element field is returned,
    *         if field is valid, else the empty list is returned.
    */
  def reorder(field: Field, squares: GenSeq[Square]): GenSeq[Field] = {
    squares match {
      case Nil => if (field.validate()) GenSeq(field) else GenSeq()
      case _ =>
        val cellCoords = field.nextEmpty()
        squares.par.map(square => {
          val newField = new Field(field.cells :+ new FieldCell(cellCoords._1, cellCoords._2, square), field.indexField)
          val newSquares = squares.filterNot( _ == square)
          (newField, newSquares)
        }).filter(_._1.validate())
          .flatMap(values => reorder(values._1, values._2))
    }
  }

  /**
    * Constructs a double-dimension array. Each cell contains
    * a non-negative number, which corresponds to a FieldCell
    * item in the cells list of the Field instance. This index
    * map provides proper field border constraints and
    * neighbour retrieval  by indexes of row and column. For
    * FieldCell with coords (i,j) the bottom-right neighbour
    * could be obtained by cells(indexField(i + 1, j + 1)),
    * the other neighbours could be retrieved the same way.
    *
    * @return index map
    * */
  def buildField(): Array[Array[Int]] = {
     Array(Array(-1, 0, 1, -1), Array(2, 3, 4, 5), Array(6, 7, 8, 9), Array(-1, 10, 11, -1))
  }

  /**
    * Obtains squares from file.
    * Returns empty collection, if file cannot be
    * read or parsed.
    *
    * The file format should look like the following:
    * a b c d
    * where a, b, c, d - digits from 0 to 9.
    *
    * @param path Path to file.
    * @return list of squares.
    */
  def getSquaresFromFile(path: String): GenSeq[Square] = {
      Try(Source.fromFile(path).getLines().map (line => {
        val digits = line.split(" ").filterNot(str => str.matches("[^0-9]")).map(_.toInt)
        assert(digits.length == 4)
        new Square(digits(0), digits(1), digits(2), digits(3))
      })) match {
        case Success(squares) => squares.toSeq
        case Failure(e) => GenSeq()
      }
  }
  /**
    * Entry point.
    */
  def main(args: Array[String]): Unit = {
    if (args.isEmpty) {
      print(USAGE)
      return
    }

    val squares =  getSquaresFromFile(args(0))
    if (squares.isEmpty) {
      print(CANNOT_READ_FILE)
      print(USAGE)
      return
    }
    val indexField = buildField()
    val data = squares.map(x => (new Field(List(new FieldCell(0,1, x)), indexField),
        squares.filterNot(s => s == x)))
    val start = System.currentTimeMillis()
    val solution = data.par.map(arg => reorder(arg._1, arg._2))
      .filterNot(list => list.isEmpty)
      .flatMap(fieldsList => fieldsList.map(field =>
          field.cells.map(cell => cell.value)
      ))
    val end = System.currentTimeMillis()
    val delay = end - start
    println(solution)
    println(TIME.format(delay))
  }
}